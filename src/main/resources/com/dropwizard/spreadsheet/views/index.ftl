<!DOCTYPE html>
<head>
    <meta charset="UTF-8">

    <script type="text/javascript" src="/assets/js/libs/angular.min.js"></script>
    <script type="text/javascript" src="/assets/js/libs/angular-route.min.js"></script>
    <script type="text/javascript" src="/assets/js/libs/angular-resource.min.js"></script>
    <script type="text/javascript" src="/assets/js/libs/math.min.js"></script>

    <script type="text/javascript" src="/assets/js/app.js"></script>
    <script type="text/javascript" src="/assets/js/routes.js"></script>

    <script type="text/javascript" src="/assets/js/controllers/sheet-controller.js"></script>
    <script type="text/javascript" src="/assets/js/controllers/application-controller.js"></script>

    <script type="text/javascript" src="/assets/js/services/sheet-service.js"></script>

    <link href="/assets/css/application.css" rel="stylesheet">
</head>
<body ng-app="SheetApp">
<div ng-view></div>
</body>
</html>
