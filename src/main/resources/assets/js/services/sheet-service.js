SheetApp.service('SheetService', ['$resource', function ($resource) {
    return $resource('/api/sheet/:id', {id: '@id'}, {
        update: {
            method: 'PUT'
        }
    })
}]);
