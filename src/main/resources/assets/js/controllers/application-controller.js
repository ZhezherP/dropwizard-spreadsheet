SheetApp.controller('ApplicationController', ['$scope', 'SheetService', function ($scope, SheetService) {
    var defaultSheet = {title: '', rows: 10, columns: 10};

    $scope.sheet = {};
    $scope.sheets = [];
    angular.extend($scope.sheet, defaultSheet);

    SheetService.query().$promise.then(function (response) {
        $scope.sheets = response;
    });

    $scope.createSheet = function () {
        SheetService.save($scope.sheet).$promise.then(function (response) {
            $scope.sheets.push(response);
            angular.extend($scope.sheet, defaultSheet);
        });
    }
}]);
