SheetApp.controller('SheetController', ['$scope', '$timeout', '$routeParams', 'SheetService',
    function ($scope, $timeout, $routeParams, SheetService) {
        var SAVE_TIMEOUT = 500;
        var savingPromise;
        var splitColAndRowRegex = /^[a-z]+|\d+$/g;

        $scope.cols = [];
        $scope.rows = [];
        $scope.errors = {};
        $scope.values = {};

        SheetService.get({id: $routeParams.id}).$promise.then(function (response) {
            $scope.sheet = response;
            if (!$scope.sheet.data) {
                $scope.sheet.data = {};
            } else {
                angular.copy($scope.sheet.data, $scope.values);
                angular.forEach($scope.values, function (val, key) {
                    var match = key.match(splitColAndRowRegex);
                    $scope.calculate(match[0], match[1]);
                });
            }
            var lastColumn = String.fromCharCode('a'.charCodeAt() + $scope.sheet.columns);
            makeRange($scope.cols, 'a', lastColumn);
            makeRange($scope.rows, 1, $scope.sheet.rows);
        });

        // UP(38), DOWN(40), ENTER(13)
        $scope.keydown = function (event, col, row) {
            switch (event.which) {
                case 38:
                    focusCell(col, row, -1);
                    break;
                case 40:
                    focusCell(col, row, 1);
                    break;
                case 13:
                    var direction = (event.shiftKey) ? -1 : 1;
                    focusCell(col, row, direction);
                    break;
            }
        };

        $scope.reset = function () {
            $scope.errors = {};
            $scope.values = {};
            $scope.sheet.data = {};
            SheetService.update($scope.sheet);
        };

        $scope.calculate = function (col, row) {
            var cellValue = $scope.sheet.data[col + row];
            if (cellValue) {
                if (isFormula(cellValue)) {
                    try {
                        $scope.values[col + row] = applyFormula(cellValue.slice(1));
                    } catch (err) {
                        $scope.values[col + row] = cellValue;
                        alert('Invalid formula format!');
                    }

                } else {
                    if (isNaN(cellValue)) {
                        alert('Acceptable only digits or formula!');
                        delete $scope.sheet.data[col + row];
                    } else {
                        $scope.values[col + row] = cellValue;
                    }
                }
            } else {
                delete $scope.values[col + row];
            }
            scheduleSave();
        };

        function scheduleSave() {
            $timeout.cancel(savingPromise);
            savingPromise = $timeout(function () {
                SheetService.update($scope.sheet);
            }, SAVE_TIMEOUT);
        }

        function makeRange(array, cur, end) {
            while (cur <= end) {
                array.push(cur);
                cur = (isNaN(cur) ? String.fromCharCode(cur.charCodeAt() + 1) : cur + 1);
            }
        }

        function isFormula(value) {
            return value[0] === '=';
        }

        function applyFormula(value) {
            var node = math.parse(value.toLowerCase());
            var code = node.compile();

            return code.eval($scope.sheet.data);
        }

        function focusCell(col, row, direction) {
            $timeout(function () {
                var cell = document.querySelector('#' + col + (row + direction));
                if (cell) {
                    cell.focus();
                }
            });
        }
    }]);
