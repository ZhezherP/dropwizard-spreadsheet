SheetApp.config(function ($routeProvider, $locationProvider) {
    $routeProvider
        .when('/', {
            templateUrl: 'assets/views/select-or-create.html',
            controller: 'ApplicationController'
        })
        .when('/sheet/:id', {
            templateUrl: '/assets/views/sheet.html',
            controller: 'SheetController'
        })
        .otherwise({
            redirectTo: '/'
        });

    if (window.history && window.history.pushState) {
        $locationProvider.html5Mode({
            enabled: true,
            requireBase: false
        });
    }
});
