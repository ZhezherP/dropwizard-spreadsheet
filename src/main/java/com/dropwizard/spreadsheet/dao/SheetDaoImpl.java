package com.dropwizard.spreadsheet.dao;

import com.dropwizard.spreadsheet.api.FullSheet;
import com.dropwizard.spreadsheet.api.Sheet;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import org.bson.BSONObject;
import org.bson.types.ObjectId;

import java.util.List;

public class SheetDaoImpl implements SheetDao {

    private final String SHEET_COLLECTION_NAME = "sheets";
    private final DBCollection collection;
    private final ObjectMapper mapper;
    private DB db;

    public SheetDaoImpl(DB db) {
        this.db = db;
        collection = db.getCollection(SHEET_COLLECTION_NAME);
        mapper = new ObjectMapper();
    }


    @Override
    public BSONObject createOrUpdate(Sheet sheet) {
        BasicDBObject object = toDBObject(sheet);
        collection.save(object);

        return setModelId(object);
    }

    @Override
    public List list() {
        BasicDBObject projection = new BasicDBObject();
        projection.put("_id", 1);
        projection.put("title", 1);

        List<DBObject> result = collection.find(new BasicDBObject(), projection).toArray();
        result.stream().forEach(this::setModelId);

        return result;
    }

    @Override
    public void delete(Sheet sheet) {
        collection.remove(toDBObject(sheet));
    }

    @Override
    public BSONObject getById(String id) {
        return setModelId(collection.findOne(new ObjectId(id)));
    }

    @Override
    public void save(FullSheet sheet) {
        collection.save(toDBObject(sheet));
    }

    private BSONObject setModelId(BSONObject obj) {
        obj.put("id", obj.removeField("_id").toString());
        return obj;
    }

    private BasicDBObject setDBId(BasicDBObject obj) {
        if (obj.get("id") != null) {
            obj.put("_id", new ObjectId(obj.removeField("id").toString()));
        } else {
            obj.removeField("id");
        }
        return obj;
    }

    private BasicDBObject toDBObject(Sheet sheet) {
        return setDBId(mapper.convertValue(sheet, BasicDBObject.class));
    }
}
