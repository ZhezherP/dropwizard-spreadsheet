package com.dropwizard.spreadsheet.dao;

import com.dropwizard.spreadsheet.api.FullSheet;
import com.dropwizard.spreadsheet.api.Sheet;
import org.bson.BSONObject;

import java.util.List;

public interface SheetDao {

    BSONObject createOrUpdate(Sheet sheet);

    List list();

    void delete(Sheet sheet);

    BSONObject getById(String id);

    void save(FullSheet sheet);
}
