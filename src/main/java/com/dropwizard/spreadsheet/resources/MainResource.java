package com.dropwizard.spreadsheet.resources;

import com.dropwizard.spreadsheet.views.MainPageView;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/")
@Produces(MediaType.TEXT_HTML)
public class MainResource {

    @GET
    public MainPageView main() {
        return new MainPageView();
    }
}
