package com.dropwizard.spreadsheet.resources;

import com.dropwizard.spreadsheet.api.FullSheet;
import com.dropwizard.spreadsheet.api.Sheet;
import com.dropwizard.spreadsheet.dao.SheetDao;
import org.bson.BSONObject;

import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/api/sheet")
@Produces(MediaType.APPLICATION_JSON)
public class SheetResource {

    private SheetDao sheetDao;

    public SheetResource(SheetDao sheetDao) {
        this.sheetDao = sheetDao;
    }

    @POST
    public BSONObject create(@Valid Sheet sheet) {
        return sheetDao.createOrUpdate(sheet);
    }

    @GET
    public List list() {
        return sheetDao.list();
    }

    @GET
    @Path("/{id}")
    public BSONObject getById(@PathParam("id") String id) {
        return sheetDao.getById(id);
    }

    @PUT
    @Path("/{id}")
    public void save(@Valid FullSheet sheet) {
        sheetDao.save(sheet);
    }
}
