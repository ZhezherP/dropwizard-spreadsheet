package com.dropwizard.spreadsheet;

import com.dropwizard.spreadsheet.dao.SheetDao;
import com.dropwizard.spreadsheet.dao.SheetDaoImpl;
import com.dropwizard.spreadsheet.providers.NotFoundProvider;
import com.dropwizard.spreadsheet.resources.MainResource;
import com.dropwizard.spreadsheet.resources.SheetResource;
import com.meltmedia.dropwizard.mongo.MongoBundle;
import com.mongodb.DB;
import io.dropwizard.Application;
import io.dropwizard.assets.AssetsBundle;
import io.dropwizard.configuration.EnvironmentVariableSubstitutor;
import io.dropwizard.configuration.SubstitutingSourceProvider;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import io.dropwizard.views.ViewBundle;

public class SpreadSheetApplication extends Application<SpreadSheetConfiguration> {

    private MongoBundle<SpreadSheetConfiguration> mongoBundle;

    public static void main(String[] args) throws Exception {
        new SpreadSheetApplication().run(args);
    }

    @Override
    public String getName() {
        return "spreadsheet";
    }

    @Override
    public void initialize(Bootstrap<SpreadSheetConfiguration> bootstrap) {
        bootstrap.setConfigurationSourceProvider(
                new SubstitutingSourceProvider(
                        bootstrap.getConfigurationSourceProvider(),
                        new EnvironmentVariableSubstitutor(false)
                )
        );
        bootstrap.addBundle(new ViewBundle<>());
        bootstrap.addBundle(new AssetsBundle());
        bootstrap.addBundle(mongoBundle = MongoBundle.<SpreadSheetConfiguration>builder()
                .withConfiguration(SpreadSheetConfiguration::getMongo)
                .build());
    }

    @Override
    public void run(SpreadSheetConfiguration configuration, Environment environment) {
        final DB db = mongoBundle.getDB();
        final SheetDao sheetDao = new SheetDaoImpl(db);


        final MainResource mainResource = new MainResource();
        final SheetResource sheetResource = new SheetResource(sheetDao);

        environment.jersey().register(mainResource);
        environment.jersey().register(sheetResource);
        environment.jersey().register(new NotFoundProvider());

    }
}
