package com.dropwizard.spreadsheet.providers;

import com.dropwizard.spreadsheet.views.MainPageView;

import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class NotFoundProvider implements ExceptionMapper<NotFoundException> {

    public Response toResponse(NotFoundException exception) {
        return Response.status(Response.Status.OK)
                .entity(new MainPageView())
                .type(MediaType.TEXT_HTML)
                .build();
    }
}
