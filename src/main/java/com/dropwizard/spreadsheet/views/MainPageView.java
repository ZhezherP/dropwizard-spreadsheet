package com.dropwizard.spreadsheet.views;

import io.dropwizard.views.View;

public class MainPageView extends View {

    public MainPageView() {
        super("index.ftl");
    }
}
