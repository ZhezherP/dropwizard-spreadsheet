package com.dropwizard.spreadsheet.api;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FullSheet extends Sheet {

    @JsonProperty
    private Object data;

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
