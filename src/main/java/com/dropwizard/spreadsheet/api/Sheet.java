package com.dropwizard.spreadsheet.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.Range;

public class Sheet {

    @JsonProperty
    private String id;

    @JsonProperty
    @NotEmpty
    private String title;

    @JsonProperty
    @Range(min = 1, max = 20)
    private int rows;

    @JsonProperty
    @Range(min = 1, max = 20)
    private int columns;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getRows() {
        return rows;
    }

    public void setRows(int rows) {
        this.rows = rows;
    }

    public int getColumns() {
        return columns;
    }

    public void setColumns(int columns) {
        this.columns = columns;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
